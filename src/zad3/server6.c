/*
 * Data:                2009-02-10
 * Autor:               Jakub Gasior <quebes@mars.iti.pk.edu.pl>
 * Kompilacja:          $ gcc server1.c -o server1
 * Uruchamianie:        $ ./server1 <numer portu>
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h> /* socket() */
#include <netinet/in.h> /* struct sockaddr_in */
#include <arpa/inet.h>  /* inet_ntop() */
#include <unistd.h>     /* close() */
#include <string.h>
#include <time.h>
#include <errno.h>
#include <netdb.h>

int main(int argc, char** argv) {
    
    int             listenfd, connfd;
    int             retval;
    struct          sockaddr_in6 client_addr, server_addr;
    socklen_t       client_addr_len, server_addr_len;
    char            buff[256];
    char            addr_buff[256];
    char            host[NI_MAXHOST],serv[NI_MAXSERV];


    if (argc != 2) {
        fprintf(stderr, "Invocation: %s <PORT>\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    listenfd = socket(PF_INET6, SOCK_STREAM, 0);
    if (listenfd == -1) {
        perror("socket()");
        exit(EXIT_FAILURE);
    }
    
    memset(&server_addr, 0, sizeof(server_addr));
    server_addr.sin6_family          =       AF_INET6;
    server_addr.sin6_addr     =       in6addr_any;
    server_addr.sin6_port            =       htons(atoi(argv[1]));
    server_addr_len                 =       sizeof(server_addr);

    if (bind(listenfd, (struct sockaddr*) &server_addr, server_addr_len) == -1) {
        perror("bind()");
        exit(EXIT_FAILURE);
    }
    
    if (listen(listenfd, 2) == -1) {
        perror("listen()");
        exit(EXIT_FAILURE);
    }
    
    while(1){

        fprintf(stdout, "Server is listening for incoming connection...\n");
    
        client_addr_len = sizeof(client_addr);
        connfd = accept(listenfd, (struct sockaddr*)&client_addr, &client_addr_len);
        if (connfd == -1) {
            perror("accept()");
            exit(EXIT_FAILURE);
        }
        
        if(IN6_IS_ADDR_V4MAPPED(&client_addr.sin6_addr)){
            printf("Adres IPv6!\n");
        } else {
            printf("Adres IPv4!\n");

        }

        /*fprintf(
            stdout, "TCP connection accepted from %s:%d\n",
            inet_ntop(AF_INET6, &client_addr.sin6_addr, addr_buff, sizeof(addr_buff)),
            ntohs(client_addr.sin6_port)
        );*/

        getnameinfo((struct sockaddr*)&client_addr, client_addr_len, host, NI_MAXHOST, serv, NI_MAXHOST, NI_NUMERICHOST|NI_NUMERICSERV);
        fprintf(
                stdout, "TCP connection accepted from %s port %s\n",
                host,serv
        );

        retval = recvfrom(
                 connfd,
                 buff, sizeof(buff),
                 0,
                 (struct sockaddr*)&client_addr, &client_addr_len
             );
        
        printf("%s\n",buff);
        
        retval = sendto(
                 connfd,
                 buff, retval,
                 0,
                 (struct sockaddr*)&client_addr, client_addr_len
             );
        
        if (retval == 0) {
            fprintf(stdout, "Connection terminated by client "
                    "(received FIN, entering CLOSE_WAIT state on connected socked)...\n");
        }
        
        if(buff[0]=='\0') break;

    }

    fprintf(stdout, "Closing connected socket (sending FIN to client)...\n");
    close(connfd);

    fprintf(stdout, "Closing listening socket and terminating server...\n");
    close(listenfd);

    sleep(3);

    exit(EXIT_SUCCESS);
}
